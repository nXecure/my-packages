Format: 1.0
Source: connman
Binary: connman, connman-iptables, connman-nftables, connman-vpn, connman-dev, connman-tests, connman-doc
Architecture: linux-any all
Version: 1.39+xecurexp-1
Maintainer: Alexander Sack <asac@debian.org>
Uploaders: Alf Gaida <agaida@siduction.org>
Homepage: https://01.org/connman
Standards-Version: 4.4.1
Vcs-Browser: https://salsa.debian.org/debian/connman
Vcs-Git: https://salsa.debian.org/debian/connman.git
Build-Depends: debhelper-compat (= 12), gtk-doc-tools, libbluetooth-dev, libdbus-1-dev, libglib2.0-dev, libgnutls28-dev, libreadline-dev, libudev-dev, libxtables-dev, libnftnl-dev, libmnl-dev, ppp-dev (>= 2.4.7-2+), wpasupplicant
Package-List:
 connman deb net optional arch=all
 connman-dev deb devel optional arch=linux-any
 connman-doc deb doc optional arch=all
 connman-iptables deb net optional arch=linux-any
 connman-nftables deb net optional arch=linux-any
 connman-tests deb net optional arch=all
 connman-vpn deb net optional arch=linux-any
Checksums-Sha1:
 1de7e6d4b092389f1a76c8990fce943b141da242 740953 connman_1.39+xecurexp.orig.tar.gz
 a0fddd5458378c1bf3c10dd2f5c060d1347741ed 20 connman_1.39+xecurexp-1.diff.gz
Checksums-Sha256:
 0f0c58600b5afb5646f0f3916542bf4f96fb8ba25bb4d620cd42c36734b6460c 740953 connman_1.39+xecurexp.orig.tar.gz
 f61f27bd17de546264aa58f40f3aafaac7021e0ef69c17f6b1b4cd7664a037ec 20 connman_1.39+xecurexp-1.diff.gz
Files:
 42f292f48c54cf85aee1039d3cece364 740953 connman_1.39+xecurexp.orig.tar.gz
 4a4dd3598707603b3f76a2378a4504aa 20 connman_1.39+xecurexp-1.diff.gz
