Format: 3.0 (quilt)
Source: badwolf
Binary: badwolf
Architecture: any
Version: 1.0.3-4
Maintainer: nXecure <not@telling.you>
Homepage: https://hacktivis.me/projects/badwolf
Standards-Version: 4.1.3
Build-Depends: debhelper (>= 11), libsoup2.4-dev, libwebkit2gtk-4.0-dev, libxml2-dev
Package-List:
 badwolf deb web optional arch=any
Checksums-Sha1:
 abd76fd2bb741592146fa4f09b6b62adda94ee6d 81711 badwolf_1.0.3.orig.tar.gz
 c28ba65daab2984cb853cd6c38af65c35c6c66a5 2336 badwolf_1.0.3-4.debian.tar.xz
Checksums-Sha256:
 81fc759a6eb579770c42c1830d9cece234d541739f2e86fe674c90f8deca5808 81711 badwolf_1.0.3.orig.tar.gz
 bcb8c43810b52f94b6f9f00386f18a50f069212d0179a176b8b14ab9ba02fb23 2336 badwolf_1.0.3-4.debian.tar.xz
Files:
 c87965a9091d5b731c6537ce70fa4877 81711 badwolf_1.0.3.orig.tar.gz
 ab6810cb372ee9691854e563fc404ced 2336 badwolf_1.0.3-4.debian.tar.xz
