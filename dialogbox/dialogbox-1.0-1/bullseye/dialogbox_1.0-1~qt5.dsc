Format: 3.0 (quilt)
Source: dialogbox
Binary: dialogbox
Architecture: any
Version: 1.0-1~qt5
Maintainer: nXecure <not@telling.you>
Homepage: https://github.com/martynets/dialogbox
Standards-Version: 4.1.3
Build-Depends: debhelper (>= 11), qtbase5-dev
Package-List:
 dialogbox deb x11 optional arch=any
Checksums-Sha1:
 c7bde855ecaab4accd25432a2a8cb7e53ec0cacf 433540 dialogbox_1.0.orig.tar.gz
 53bb6c1df1507e11a00b972cd25f772685b9c5f9 1368 dialogbox_1.0-1~qt5.debian.tar.xz
Checksums-Sha256:
 fbf4fec5b6383b8969a9eb3108c3f1c40ec8fbb04e74f083c3319ba736d4fc66 433540 dialogbox_1.0.orig.tar.gz
 c58085a6bd76f4a803aef45b01348e833eb8a178a6e7458579527447ee0c413f 1368 dialogbox_1.0-1~qt5.debian.tar.xz
Files:
 ab666bb08788ee33bb7e0580a90352a1 433540 dialogbox_1.0.orig.tar.gz
 f0a74e4a981834d2517f7d76695bcddd 1368 dialogbox_1.0-1~qt5.debian.tar.xz
