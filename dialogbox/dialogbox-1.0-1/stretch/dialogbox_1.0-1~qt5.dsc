Format: 3.0 (quilt)
Source: dialogbox
Binary: dialogbox
Architecture: any
Version: 1.0-1~qt5
Maintainer: nXecure <not@telling.you>
Homepage: https://github.com/martynets/dialogbox
Standards-Version: 4.1.3
Build-Depends: debhelper (>= 10), qtbase5-dev
Package-List:
 dialogbox deb x11 optional arch=any
Checksums-Sha1:
 a5f39147cf33a907c2bcabe94501f06f15837020 433548 dialogbox_1.0.orig.tar.gz
 dff72b5c515b9651c6d2f0a3dc9a7979c4c6786d 1372 dialogbox_1.0-1~qt5.debian.tar.xz
Checksums-Sha256:
 02a9b1fdb1827967cbb68051cb89698401e9cce6844dd1a644afbb5d9451e42b 433548 dialogbox_1.0.orig.tar.gz
 fa828ff9b02bdad7c20f6cfd70e70c193e2dc4f0c2c3c562379b4344b0e6406c 1372 dialogbox_1.0-1~qt5.debian.tar.xz
Files:
 4f093a44d2cac24445b2bb373fcaca8b 433548 dialogbox_1.0.orig.tar.gz
 5e626ac8046454ce28d3b175841bfcf5 1372 dialogbox_1.0-1~qt5.debian.tar.xz
