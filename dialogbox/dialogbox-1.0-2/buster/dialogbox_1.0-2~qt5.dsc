Format: 3.0 (quilt)
Source: dialogbox
Binary: dialogbox
Architecture: any
Version: 1.0-2~qt5
Maintainer: nXecure <not@telling.you>
Homepage: https://github.com/martynets/dialogbox
Standards-Version: 4.1.3
Build-Depends: debhelper (>= 11), qtbase5-dev
Package-List:
 dialogbox deb x11 optional arch=any
Checksums-Sha1:
 1e7e030e65a2a9f7d861a6dd0544bcd29238eaed 433602 dialogbox_1.0.orig.tar.gz
 7e3aa930dc99950128e63b7c6340a31b5a139883 1424 dialogbox_1.0-2~qt5.debian.tar.xz
Checksums-Sha256:
 0256ab309ae641b85d55e2993b5fa4ad83648b3737faac273a59baa8396791c5 433602 dialogbox_1.0.orig.tar.gz
 213ffeda07572f6a8fb3e5cf67c116f50e38a17a9712d16ad2f7f48b31a233a6 1424 dialogbox_1.0-2~qt5.debian.tar.xz
Files:
 4abcfa8fdc313bf2cda6044f89555308 433602 dialogbox_1.0.orig.tar.gz
 64dadf717af088e078a0c12db93e9ad9 1424 dialogbox_1.0-2~qt5.debian.tar.xz
