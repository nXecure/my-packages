Format: 3.0 (quilt)
Source: dialogbox
Binary: dialogbox
Architecture: any
Version: 1.0-2~qt5
Maintainer: nXecure <not@telling.you>
Homepage: https://github.com/martynets/dialogbox
Standards-Version: 4.1.3
Build-Depends: debhelper (>= 10), qtbase5-dev
Package-List:
 dialogbox deb x11 optional arch=any
Checksums-Sha1:
 11beb449bc1b4908fd855e587bad0fb51a80cfa6 433613 dialogbox_1.0.orig.tar.gz
 c2a9a737bf0fb140739c4372dce828c073e5ebcb 1432 dialogbox_1.0-2~qt5.debian.tar.xz
Checksums-Sha256:
 77fb26f04baa6ce5cc022dd1611fc806387215e3239f589c94c3aca0e9dfe63f 433613 dialogbox_1.0.orig.tar.gz
 98ae3b89609accc1e8d56a9418e14bf1847aa27e8e95ef387d5d13f514b7687a 1432 dialogbox_1.0-2~qt5.debian.tar.xz
Files:
 139719ebe8369dc4ae947b17ca0b33f1 433613 dialogbox_1.0.orig.tar.gz
 7079e4ad0083dac7006db93619bd0fb2 1432 dialogbox_1.0-2~qt5.debian.tar.xz
