Format: 3.0 (quilt)
Source: dialogbox
Binary: dialogbox
Architecture: any
Version: 1.0-2~qt5
Maintainer: nXecure <not@telling.you>
Homepage: https://github.com/martynets/dialogbox
Standards-Version: 4.1.3
Build-Depends: debhelper (>= 11), qtbase5-dev
Package-List:
 dialogbox deb x11 optional arch=any
Checksums-Sha1:
 6c9355f40b4dfbd7740784ec54dc2f41b4a173d7 433602 dialogbox_1.0.orig.tar.gz
 7e3aa930dc99950128e63b7c6340a31b5a139883 1424 dialogbox_1.0-2~qt5.debian.tar.xz
Checksums-Sha256:
 0e2371ee6952573a5ae27144405f330fa68b0e56fe1fcb7522bf075a853e5aee 433602 dialogbox_1.0.orig.tar.gz
 213ffeda07572f6a8fb3e5cf67c116f50e38a17a9712d16ad2f7f48b31a233a6 1424 dialogbox_1.0-2~qt5.debian.tar.xz
Files:
 cf1bfa026d9c3f64eb3f0d5f1f8abaa1 433602 dialogbox_1.0.orig.tar.gz
 64dadf717af088e078a0c12db93e9ad9 1424 dialogbox_1.0-2~qt5.debian.tar.xz
