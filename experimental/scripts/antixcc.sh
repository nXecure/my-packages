#!/bin/bash
# File Name: controlcenter.sh
# Purpose: all-in-one control centre for antiX
# Authors: OU812 and minor modifications by anticapitalista
# Latest Change:
# 20 August 2008
# 11 January 2009 and renamed antixcc.sh
# 15 August 2009 some apps and labels altered.
# 09 March 2012 by anticapitalista. Added Live section.
# 22 March 2012 by anticapitalista. Added jwm config options and edited admin options.
# 18 April 2012 by anticapitalista. mountbox-antix opens as user not root.
# 06 October 2012 by anticapitalista. Function for ICONS. New icon theme.
# 26 October 2012 by anticapitalista. Includes gksudo and ktsuss.
# 12 May 2013 by anticapitalista. Let user set default apps.
# 05 March 2015 by BitJam: Add alsa-set-card, edit excludes, edit bootloader.  Fix indentation.
#   * Hide live tab on non-live systems.  Use echo instead of gettext.
#   * Remove unneeded doublequotes between tags.  Use $(...) instead of `...`.
# 01 May 2016 by anticapitalista: Use 1 script and use hides if nor present on antiX-base
# 11 July 2017 by BitJam:
#   * use a subroutine to greatly consolidate code
#   * use existence of executable as the key instead of documentation directory
#     perhaps I should switch to "which" or "type"
#   * move set-dpi to desktop tab
#   * enable ati driver button in hardware tab
# 18 Nov by antiX-Dave: fix edit jwm settings button to match icons with icewm and fluxbox
# 19 Nov 2020 - Remade using dialogbox and yad (prototype)
# 
# Acknowledgements: Original script by KDulcimer of TinyMe. http://tinyme.mypclinuxos.com
#################################################################################################################################################

TEXTDOMAINDIR=/usr/share/locale
TEXTDOMAIN=antixcc.sh
# Options
ICONS=/usr/share/icons/antix-papirus
ICONS2=/usr/share/pixmaps
CC_COLUMNS=3 # transforms to ROWS if FRAME_LAYOUT=horizontal
ROW_ELEMENTS=1 # if FRAME_LAYOUT=horizontal, lets you control amount of consecutive elements per row

USE_TABS=true # use tabs instead, to make transitions better
HIDE_TABBAR=true

FRAME_LAYOUT="vertical" # can be "horizontal" or "vertical"

TEXT_FOR_BUTTON="outside" # Text can be "inside" the button or "outside" it
BUTTON_ICONS=true # Do you want icons inside the Buttons?
BUTTON_ICONSIZE=48 # iconsize of icons inside buttons (from 10 to 48)

BUTTON_STRETCH=false # creates a separation between items inside the frames (works best on vertical)
FIRST_STRETCH=true
LAST_STRETCH=true # only works for FRAME_LAYOUT=vertical
LEFT_STRETCH=false # enables the first vertical layout stretch from the listbox

DIALOG_RESIZE=true # if true, the dialog can be resized.

EXCLUDES_DIR=/usr/local/share/excludes

#check if using desktop-defaults (antiX only)
if [ -x /usr/local/bin/desktop-defaults-run ]; then
	terminalexec="desktop-defaults-run -t"
	editorexec="desktop-defaults-run -te"
	echo "desktop-session-antix installed"
else
	terminalexec="x-terminal-emulator -e"
	editorexec="xdg-open"
	echo "no desktop-defaults-run"
fi

# TAB titles
Desktop=$"Desktop" System=$"System" Network=$"Network" Shares=$"Shares" Session=$"Session"
Live=$"Live" Disks=$"Disks" Hardware=$"Hardware" Drivers=$"Drivers" Maintenance=$"Maintenance"

# Select first tab based on input
SHOW_TAB="$Desktop" # Default tab
CC_INPUT="$@"
  case $CC_INPUT in
	  --desktop)     SHOW_TAB="$Desktop" ;;
	  --system)      SHOW_TAB="$System" ;;
	  --network)     SHOW_TAB="$Network" ;;
	  --shares)      SHOW_TAB="$Shares" ;;
	  --session)     SHOW_TAB="$Session" ;;
	  --live)        SHOW_TAB="$Live" ;;
	  --disks)       SHOW_TAB="$Disks" ;;
	  --hardware)    SHOW_TAB="$Hardware" ;;
	  --drivers)     SHOW_TAB="$Drivers" ;;
	  --maintenance) SHOW_TAB="$Maintenance" ;;
  esac

# TAB icons
DESKTOP_ICON="$ICONS/preferences-desktop-wallpaper.png" #"/usr/share/icons/papirus-antix/48x48/places/desktop.png"
SYSTEM_ICON="$ICONS/choose-startup-services.png" #"/usr/share/icons/papirus-antix/48x48/devices/computer.png"
NETWORK_ICON="$ICONS/nm-device-wireless.png" #"/usr/share/icons/papirus-antix/48x48/devices/network-wireless.png"
SHARES_ICON="$ICONS/connect.png" #"/usr/share/icons/Adwaita/48x48/status/network-receive.png"
SESSION_ICON="$ICONS/preferences-system-session.png" #"/usr/share/icons/papirus-antix/48x48/apps/preferences-desktop-screensaver.png"
LIVE_ICON="$ICONS/live-usb-kernel-updater.png" #"/usr/share/icons/papirus-antix/48x48/devices/media-flash-memory-stick.png"
DISKS_ICON="$ICONS/gparted.png" #"/usr/share/icons/papirus-antix/48x48/devices/drive-harddisk.png"
HARDWARE_ICON="$ICONS/soundcard.png" #"/usr/share/icons/papirus-antix/48x48/devices/audio-card.png"
DRIVERS_ICON="$ICONS/preferences-system-login.png" #"/usr/share/icons/papirus-antix/48x48/mimetypes/application-x-addon.png"
MAINTENANCE_ICON="$ICONS/preferences-system.png" #"/usr/share/icons/papirus-antix/48x48/apps/gnome-settings.png"
CONFIGURATION_ICON="$ICONS/applications-system.png" 

# Check if the system is running live
its_alive() {
    # return 0
    local root_fstype=$(df -PT / | tail -n1 | awk '{print $2}')
    case $root_fstype in
        aufs|overlay) return 0 ;;
                   *) return 1 ;;
    esac
}

# Function to build each tab's content
build_layout(){
    local LAYOUT_NAME="${1}"
    local TAB_ICON="${2}"
    shift 2
    local PROGRAM_LIST="$@"
TEMP_LAYOUT=$(mktemp -p /dev/shm)
    # Check that there are buttons that will be drawn
    if [ ! -z "$DESKTOP_PROGRAMS" ]; then
		TOTAL_PROG=$(wc -l <(echo "$PROGRAM_LIST") | awk '{ print $1 }')
		TOTAL_PROG=$((--TOTAL_PROG))
		# Create tab
		if $USE_TABS && ! $HIDE_TABBAR; then 
			echo "add page \"\" ${LAYOUT_NAME}_TAB $TAB_ICON" >> $TEMP_LAYOUT
		elif $USE_TABS; then echo "add page \"\" ${LAYOUT_NAME}_TAB" >> $TEMP_LAYOUT; fi
        # Create one frame per column
        for (( i=1; i<=$CC_COLUMNS; i++ )); do
            echo "add frame ${LAYOUT_NAME}_${i} $FRAME_LAYOUT noframe" >> $TEMP_LAYOUT
            if $FIRST_STRETCH && $BUTTON_STRETCH; then echo "add stretch" >> $TEMP_LAYOUT; fi
            echo "end frame ${LAYOUT_NAME}_${i}" >> $TEMP_LAYOUT
            if ! $USE_TABS; then echo "hide ${LAYOUT_NAME}_${i}" >> $TEMP_LAYOUT; fi
            if [[ "$FRAME_LAYOUT" == "vertical" ]]; then
				echo "step horizontal" >> $TEMP_LAYOUT; fi
        done
        # End tab
        if $USE_TABS; then echo "end page ${LAYOUT_NAME}_TAB" >> $TEMP_LAYOUT; fi
        
        COLUMN_COUNT=1
        ROW_COUNT=1
        PROG_COUNTDOWN=$TOTAL_PROG
        while read -r line; do
            # non-empty line
            if [ ! -z "$line" ]; then
                echo "position onto ${LAYOUT_NAME}_${COLUMN_COUNT}" >> $TEMP_LAYOUT
                
                BUTTON_NAME="$(echo $line | cut -d" " -f1)"
                BUTTON_ICON="$(echo $line | cut -d" " -f2)"
                BUTTON_TEXT="$(echo $line | cut -d" " -f3-)"
                if [[ "$TEXT_FOR_BUTTON" == "inside" ]]; then
                    echo "add pushbutton \"$BUTTON_TEXT\" $BUTTON_NAME" >> $TEMP_LAYOUT
                else
                    echo "add pushbutton \"\" $BUTTON_NAME" >> $TEMP_LAYOUT
                    echo "add label \"$BUTTON_TEXT\"\"${BUTTON_NAME}_LABEL\"" >> $TEMP_LAYOUT
                    echo "set \"${BUTTON_NAME}_LABEL\" stylesheet \"qproperty-alignment: AlignCenter;\"" >> $TEMP_LAYOUT
                fi
                if $BUTTON_ICONS; then
					echo "set $BUTTON_NAME icon $BUTTON_ICON" >> $TEMP_LAYOUT; fi
				if $BUTTON_STRETCH; then
					if [[ "$FRAME_LAYOUT" == "vertical" ]]; then
						if [ $PROG_COUNTDOWN -gt $CC_COLUMNS ]; then
							echo "add stretch" >> $TEMP_LAYOUT
						elif $LAST_STRETCH; then echo "add stretch" >> $TEMP_LAYOUT; fi
					else echo "add stretch" >> $TEMP_LAYOUT; fi
				fi
                # Move to the next column and program
                PROG_COUNTDOWN=$((--PROG_COUNTDOWN))
                if [[ "$FRAME_LAYOUT" == "vertical" ]]; then 
					COLUMN_COUNT=$((++COLUMN_COUNT))
				else
					ROW_COUNT=$((++ROW_COUNT))
				fi
                if [ $ROW_COUNT -gt $ROW_ELEMENTS ]; then
					COLUMN_COUNT=$((++COLUMN_COUNT));
					ROW_COUNT=1
				fi
                if [ $COLUMN_COUNT -gt $CC_COLUMNS ]; then COLUMN_COUNT=1; fi
            fi
        done < <(echo "$PROGRAM_LIST")
    fi
    cat "$TEMP_LAYOUT"
rm -f -- "$TEMP_LAYOUT"
}

# Switch between layouts/tabs
show_layout(){
    local SHOW_LAYOUT="${1}"
    local HIDE_LAYOUT="${2}"
    local LAYOUT_NAME
    local SHOW_NAME
    local HIDE_NAME
    local TEMP_LAYOUT=$(mktemp -p /dev/shm)
    
    for tab in "$SHOW_LAYOUT" "$HIDE_LAYOUT"; do
		LAYOUT_NAME=""
		case "$tab" in 
			    "$Desktop")   LAYOUT_NAME="DESKTOP"     ;;
			     "$System")   LAYOUT_NAME="SYSTEM"      ;;
			    "$Network")   LAYOUT_NAME="NETWORK"     ;;
			     "$Shares")   LAYOUT_NAME="SHARES"      ;;
			    "$Session")   LAYOUT_NAME="SESSION"     ;;
			      "$Disks")   LAYOUT_NAME="DISKS"       ;;
			   "$Hardware")   LAYOUT_NAME="HARDWARE"    ;;
			    "$Drivers")   LAYOUT_NAME="DRIVERS"     ;;
			"$Maintenance")   LAYOUT_NAME="MAINTENANCE" ;;
			       "$Live")   LAYOUT_NAME="LIVE"        ;;
		esac
		
		if [[ "$tab" == "$SHOW_LAYOUT" ]]; then
			SHOW_NAME="$LAYOUT_NAME"
		elif [ ! -z "$tab" ] && [[ "$tab" == "$HIDE_LAYOUT" ]]; then
			HIDE_NAME="$LAYOUT_NAME"
		fi
    done
    
    # Hide selected layout
    if ! $USE_TABS && [ ! -z "$HIDE_LAYOUT" ] && [ ! -z "$HIDE_NAME" ]; then
        for (( i=1; i<=$CC_COLUMNS; i++ )); do
            echo "hide ${HIDE_NAME}_${i}" >> $TEMP_LAYOUT
        done
    fi
    
    # Show seected layout
    if [ ! -z "$SHOW_LAYOUT" ] && [ ! -z "$SHOW_NAME" ]; then
		if $USE_TABS; then
			echo "set ${SHOW_NAME}_TAB current" >> $TEMP_LAYOUT
		else
			for (( i=1; i<=$CC_COLUMNS; i++ )); do
				echo "show ${SHOW_NAME}_${i}" >> $TEMP_LAYOUT
			done
        fi
        # Change list label
		echo "set LIST_TABS title \"$SHOW_LAYOUT\"" >> $TEMP_LAYOUT
    fi
    
    # Export changes to dialogbox
    cat "$TEMP_LAYOUT" >&$OUTPUTFD
rm -f -- "$TEMP_LAYOUT"
}

# open a yad window with a selection of files to open
yad_textfile(){
	local TEXT_CATEGORY="${1}"
	local FILE_LIST
	local NEED_PERM=false
	local TOTAL_FILES
	local OPEN_EDITOR="$editorexec"
	
	# Create a list of files depending on the "text category"
	case "$TEXT_CATEGORY" in
    	          icewm)  FILE_LIST="$HOME/.icewm/winoptions $HOME/.icewm/preferences \
						   $HOME/.icewm/keys $HOME/.icewm/startup $HOME/.icewm/toolbar \
						   $HOME/.icewm/menu";;
    	            jwm)  FILE_LIST="$HOME/.jwm/preferences $HOME/.jwm/keys \
    	                   $HOME/.jwm/tray $HOME/.jwm/startup $HOME/.jwmrc \
    	                   $HOME/.jwm/menu";;
    	        fluxbox)  FILE_LIST="$HOME/.fluxbox/overlay $HOME/.fluxbox/keys \
    	                   $HOME/.fluxbox/init $HOME/.fluxbox/startup \
    	                   $HOME/.fluxbox/apps $HOME/.fluxbox/menu";;
    	          conky)  FILE_LIST="$HOME/.conkyrc";;
    	   config-files)  FILE_LIST="/etc/fstab /etc/default/keyboard /etc/slim.conf"
							# get grub files
							for file in /etc/grub.d/*; do
								FILE_LIST="$FILE_LIST $file"
							done
							# get sources.list files
							for file in /etc/apt/sources.list.d/*.list; do
								FILE_LIST="$FILE_LIST $file"
							done
    	                  NEED_PERM=true;;
        desktop-session)  FILE_LIST="$HOME/.desktop-session/startup"
							# get desktop-session config files
							for file in $HOME/.desktop-session/*.conf; do
								FILE_LIST="$FILE_LIST $file"
							done;;
    	       excludes)  for file in $EXCLUDES_DIR/*.list; do
							if [ -z "$FILE_LIST" ]; then FILE_LIST="$file"
							else FILE_LIST="$FILE_LIST $file"; fi
    	                  done
    	                  NEED_PERM=true;;
    	     bootloader)  FILE_LIST="/live/boot-dev/boot/syslinux/syslinux.cfg \
    	                   /live/boot-dev/boot/grub/grub.cfg"
    	                  NEED_PERM=true;;
	esac
	
	# exit if no file found
	TOTAL_FILES=$(wc -w <(echo "$FILE_LIST") | awk '{ print $1 }')
	if [ -z "$FILE_LIST" ] || [ $TOTAL_FILES -eq 0 ]; then return 1; fi
	
	# Calculate amount of columns for yad dialog
	if [ $TOTAL_FILES -gt 14 ]; then FORM_COLUMNS=3
	elif [ $TOTAL_FILES -gt 7 ]; then FORM_COLUMNS=2
	else FORM_COLUMNS=1; fi
	
	# Editor. If Permission is needed, add sudo permissions
	if $NEED_PERM; then OPEN_EDITOR="gksu xdg-open"; fi
	export OPEN_EDITOR
	
	# if only one file, open directly 
	if [ $TOTAL_FILES -eq 1 ]; then
		bash -c "$OPEN_EDITOR $FILE_LIST &"
	# More than 2 files, build yad dialog to select
	else
		local YAD_FORM
		for word in $FILE_LIST; do
			FILE_NAME="${word##*/}"
			BTN_CMD="$OPEN_EDITOR $word"
			YAD_FORM="$YAD_FORM --field=$FILE_NAME:FBTN \"$BTN_CMD\""
		done
		
		#launch yad window
		YAD_TITLE=$"Open text file"
		OPEN_BUTTON=$"Open All"
		eval "yad --title=\"$YAD_TITLE\" --center --form --borders=10 \
		--align=center --columns=\"$FORM_COLUMNS\" $YAD_FORM \
		--button=\"$OPEN_BUTTON\":0 --button='gtk-close':1"
		local exitcode=$?
		# open all files one by one
		if [ $exitcode -eq 0 ]; then
			for word in $FILE_LIST; do
				bash -c "$OPEN_EDITOR $word &"
			done
		fi
	fi
}

# CHeck if the system is running live
if its_alive; then ITS_ALIVE=true
else ITS_ALIVE=false; fi

# Set tabs if this option is selected
if $USE_TABS; then
	TABS_START="add tabs CC_TABS top"
	TABS_END="end tabs CC_TABS"
fi

### Creating list of programs ###
## DESKTOP TAB ##
DESKTOP_PROGRAMS=""
    # WALLPAPER_BUTTON #
    WALLPAPER_PROG=/usr/local/bin/wallpaper.py
    WALLPAPER_EXEC="wallpaper.py &"
    WALLPAPER_ICON="$ICONS/preferences-desktop-wallpaper.png"
    WALLPAPER_TEXT=$"Choose Wallpaper"
    if [ -x $WALLPAPER_PROG ]; then
        DESKTOP_PROGRAMS="${DESKTOP_PROGRAMS}"$'\n'"WALLPAPER_BUTTON ${WALLPAPER_ICON} ${WALLPAPER_TEXT}"
    fi
    # DPI_BUTTON #
    DPI_PROG=/usr/local/bin/set-dpi
    DPI_EXEC="gksu set-dpi &"
    DPI_ICON="$ICONS/fonts.png"
    DPI_TEXT=$"Set Font Size"
    DPI_TEXT="$DPI_TEXT DPI"
    if [ -x $DPI_PROG ]; then
        DESKTOP_PROGRAMS="${DESKTOP_PROGRAMS}"$'\n'"DPI_BUTTON ${DPI_ICON} ${DPI_TEXT}"
    fi
    # LXAPPEARANCE_BUTTON #
    LXAPPEARANCE_PROG=/usr/bin/lxappearance
    LXAPPEARANCE_EXEC="lxappearance &"
    LXAPPEARANCE_ICON="$ICONS/preferences-desktop-theme.png"
    LXAPPEARANCE_TEXT=$"Customize Look and Feel"
    if [ -x $LXAPPEARANCE_PROG ]; then
        DESKTOP_PROGRAMS="${DESKTOP_PROGRAMS}"$'\n'"LXAPPEARANCE_BUTTON ${LXAPPEARANCE_ICON} ${LXAPPEARANCE_TEXT}"
    fi
    # ICEWM_CONFIG_BUTTON #
    ICEWM_CONFIG_PROG=/usr/share/xsessions/icewm.desktop
    ICEWM_CONFIG_ICON="$ICONS/gnome-documents.png"
    ICEWM_CONFIG_TEXT=$"Edit IceWM Settings"
    if [ -e $ICEWM_CONFIG_PROG ] && [ -d $HOME/.icewm ]; then
        DESKTOP_PROGRAMS="${DESKTOP_PROGRAMS}"$'\n'"ICEWM_CONFIG_BUTTON ${ICEWM_CONFIG_ICON} ${ICEWM_CONFIG_TEXT}"
    fi
    # JWM_CONFIG_BUTTON #
    JWM_CONFIG_PROG=/usr/share/xsessions/jwm.desktop
    JWM_CONFIG_ICON="$ICONS/gnome-documents.png"
    JWM_CONFIG_TEXT=$"Edit JWM Settings"
    if [ -e $JWM_CONFIG_PROG ] && [ -d $HOME/.jwm ]; then
        DESKTOP_PROGRAMS="${DESKTOP_PROGRAMS}"$'\n'"JWM_CONFIG_BUTTON ${JWM_CONFIG_ICON} ${JWM_CONFIG_TEXT}"
    fi
    # FLUX_CONFIG_BUTTON #
    FLUX_CONFIG_PROG=/usr/share/xsessions/fluxbox.desktop
    FLUX_CONFIG_ICON="$ICONS/gnome-documents.png"
    FLUX_CONFIG_TEXT=$"Edit Fluxbox Settings"
    if [ -e $FLUX_CONFIG_PROG ] && [ -d $HOME/.fluxbox ]; then
        DESKTOP_PROGRAMS="${DESKTOP_PROGRAMS}"$'\n'"FLUX_CONFIG_BUTTON ${FLUX_CONFIG_ICON} ${FLUX_CONFIG_TEXT}"
    fi
    # CONKY_BUTTON #
    CONKY_PROG=/usr/bin/conky
    CONKY_ICON="$ICONS/conky.png"
    CONKY_TEXT=$"Edit System Monitor (Conky)"
    if [ -x $CONKY_PROG ] && [ -w $HOME/.conkyrc ]; then
        DESKTOP_PROGRAMS="${DESKTOP_PROGRAMS}"$'\n'"CONKY_BUTTON ${CONKY_ICON} ${CONKY_TEXT}"
    fi
    # PREFAPPS_BUTTON #
    PREFAPPS_PROG=/usr/local/bin/desktop-defaults-set
    PREFAPPS_EXEC="desktop-defaults-set &"
    PREFAPPS_ICON="$ICONS/gnome-settings-default-applications.png"
    PREFAPPS_TEXT=$"Preferred Applications"
    if [ -x $PREFAPPS_PROG ]; then
        DESKTOP_PROGRAMS="${DESKTOP_PROGRAMS}"$'\n'"PREFAPPS_BUTTON ${PREFAPPS_ICON} ${PREFAPPS_TEXT}"
    fi

## SYSTEM TAB ##
SYSTEM_PROGRAMS=""
    # SYNAPTIC_BUTTON #
    SYNAPTIC_PROG=/usr/sbin/synaptic
    SYNAPTIC_EXEC="gksu synaptic &"
    SYNAPTIC_EXEC2="$terminalexec sudo /usr/local/bin/cli-aptiX --pause &"
    SYNAPTIC_ICON="$ICONS/synaptic.png"
    SYNAPTIC_TEXT=$"Manage Packages"
    if [ -x $SYNAPTIC_PROG ] || [ -x /usr/local/bin/cli-aptiX ]; then
        SYSTEM_PROGRAMS="${SYSTEM_PROGRAMS}"$'\n'"SYNAPTIC_BUTTON ${SYNAPTIC_ICON} ${SYNAPTIC_TEXT}"
    fi
    # PACKAGE_INST_BUTTON #
    PACKAGE_INST_PROG=/usr/bin/packageinstaller
    PACKAGE_INST_EXEC="gksu packageinstaller &"
    PACKAGE_INST_ICON="$ICONS/packageinstaller.png"
    PACKAGE_INST_TEXT=$"Package Installer"
    if [ -x $PACKAGE_INST_PROG ]; then
        SYSTEM_PROGRAMS="${SYSTEM_PROGRAMS}"$'\n'"PACKAGE_INST_BUTTON ${PACKAGE_INST_ICON} ${PACKAGE_INST_TEXT}"
    fi
    # CONFIG_FILES_BUTTON #
    CONFIG_FILES_ICON="$ICONS/gnome-documents.png"
    CONFIG_FILES_TEXT=$"Edit Config Files"
        SYSTEM_PROGRAMS="${SYSTEM_PROGRAMS}"$'\n'"CONFIG_FILES_BUTTON ${CONFIG_FILES_ICON} ${CONFIG_FILES_TEXT}"
    # FSKB_SETTINGS_BUTTON #
    FSKB_SETTINGS_PROG=/usr/bin/fskbsetting
    FSKB_SETTINGS_EXEC="gksu fskbsetting &"
    FSKB_SETTINGS_ICON="$ICONS/im-chooser.png"
    FSKB_SETTINGS_TEXT=$"Set System Keyboard Layout"
    if [ -x $FSKB_SETTINGS_PROG ]; then
        SYSTEM_PROGRAMS="${SYSTEM_PROGRAMS}"$'\n'"FSKB_SETTINGS_BUTTON ${FSKB_SETTINGS_ICON} ${FSKB_SETTINGS_TEXT}"
    fi
    # SYSV_CONF_BUTTON #
    SYSV_CONF_PROG=/usr/sbin/sysv-rc-conf
    SYSV_CONF_EXEC="rc-conf-wrapper.sh &"
    SYSV_CONF_ICON="$ICONS/choose-startup-services.png"
    SYSV_CONF_TEXT=$"Choose Startup Services"
    if [ -x $SYSV_CONF_PROG ]; then
        SYSTEM_PROGRAMS="${SYSTEM_PROGRAMS}"$'\n'"SYSV_CONF_BUTTON ${SYSV_CONF_ICON} ${SYSV_CONF_TEXT}"
    fi
    # TZDATA_BUTTON #
    TZDATA_PROG=/usr/local/bin/set_time-and_date.sh
    TZDATA_EXEC="set_time-and_date.sh &"
    TZDATA_EXEC2="$terminalexec sudo dpkg-reconfigure tzdata &"
    TZDATA_ICON="$ICONS/time-admin.png"
    TZDATA_TEXT=$"Set Date and Time"
    if [[ -x $TZDATA_PROG || -x /usr/sbin/dpkg-reconfigure ]] && [ -d /usr/share/zoneinfo/ ]; then
        SYSTEM_PROGRAMS="${SYSTEM_PROGRAMS}"$'\n'"TZDATA_BUTTON ${TZDATA_ICON} ${TZDATA_TEXT}"
    fi
    # GALTERNATIVES_BUTTON #
    GALTERNATIVES_PROG=/usr/bin/galternatives
    GALTERNATIVES_EXEC="galternatives &"
    GALTERNATIVES_ICON="$ICONS/galternatives.png"
    GALTERNATIVES_TEXT=$"Alternatives Configurator"
    if [ -x $GALTERNATIVES_PROG ]; then
        SYSTEM_PROGRAMS="${SYSTEM_PROGRAMS}"$'\n'"GALTERNATIVES_BUTTON ${GALTERNATIVES_ICON} ${GALTERNATIVES_TEXT}"
    fi

## NETWORK TAB ##
NETWORK_PROGRAMS=""
    # CONNMAN_BUTTON #
    CONNMAN_PROG=/usr/bin/connmanctl
    CONNMAN_EXEC="antix-wifi-switch -s connman &"
    CONNMAN_EXEC2="$terminalexec connmanctl &"
    CONNMAN_ICON="$ICONS/connman.png"
    CONNMAN_TEXT=$"WiFi Connect (Connman)"
    if [ -x $CONNMAN_PROG ]; then
        NETWORK_PROGRAMS="${NETWORK_PROGRAMS}"$'\n'"CONNMAN_BUTTON ${CONNMAN_ICON} ${CONNMAN_TEXT}"
    fi
    # CENI_BUTTON #
    CENI_PROG=/usr/sbin/ceni
    CENI_EXEC="antix-wifi-switch -s ceni &"
    CENI_EXEC2="$terminalexec sudo ceni &"
    CENI_ICON="$ICONS/ceni.png"
    CENI_TEXT=$"Network Interfaces (Ceni)"
    if [ -x $CENI_PROG ]; then
        NETWORK_PROGRAMS="${NETWORK_PROGRAMS}"$'\n'"CENI_BUTTON ${CENI_ICON} ${CENI_TEXT}"
    fi
    # WPASUPPLICANT_BUTTON #
    WPASUPPLICANT_PROG=/usr/sbin/wpa_gui
    WPASUPPLICANT_EXEC="/usr/sbin/wpa_gui &"
    WPASUPPLICANT_ICON="$ICONS/wpa_gui.png"
    WPASUPPLICANT_TEXT=$"WPA Supplicant Configuration"
    if [ -x $WPASUPPLICANT_PROG ]; then
        NETWORK_PROGRAMS="${NETWORK_PROGRAMS}"$'\n'"WPASUPPLICANT_BUTTON ${WPASUPPLICANT_ICON} ${WPASUPPLICANT_TEXT}"
    fi
    # UMTS_BUTTON #
    UMTS_PROG=/usr/bin/umts-panel
    UMTS_EXEC="umts-panel &"
    UMTS_ICON="$ICONS/nm-device-wireless.png"
    UMTS_TEXT=$"Configure GPRS/UMTS"
    if [ -x $UMTS_PROG ]; then
        NETWORK_PROGRAMS="${NETWORK_PROGRAMS}"$'\n'"UMTS_BUTTON ${UMTS_ICON} ${UMTS_TEXT}"
    fi
    # GNOMEPPP_BUTTON #
    GNOMEPPP_PROG=/usr/bin/gnome-ppp
    GNOMEPPP_EXEC="gnome-ppp &"
    GNOMEPPP_ICON="$ICONS/gnome-ppp.png"
    GNOMEPPP_TEXT=$"Dial-Up Configuaration (GNOME PPP)"
    if [ -x $GNOMEPPP_PROG ]; then
        NETWORK_PROGRAMS="${NETWORK_PROGRAMS}"$'\n'"GNOMEPPP_BUTTON ${GNOMEPPP_ICON} ${GNOMEPPP_TEXT}"
    fi
    # PPPOE_CONF_BUTTON #
    PPPOE_CONF_PROG=/usr/sbin/pppoeconf
    PPPOE_CONF_EXEC="$terminalexec /usr/sbin/pppoeconf &"
    PPPOE_CONF_ICON="$ICONS/internet-telephony.png"
    PPPOE_CONF_TEXT=$"ADSL/PPPOE Configuration"
    if [ -x $PPPOE_CONF_PROG ]; then
        NETWORK_PROGRAMS="${NETWORK_PROGRAMS}"$'\n'"PPPOE_CONF_BUTTON ${PPPOE_CONF_ICON} ${PPPOE_CONF_TEXT}"
    fi
    # FIREWALL_BUTTON #
    FIREWALL_PROG=/usr/bin/gufw
    FIREWALL_EXEC="gksu gufw &"
    FIREWALL_ICON="$ICONS/gufw.png"
    FIREWALL_TEXT=$"Firewall Configuration"
    if [ -x $FIREWALL_PROG ]; then
        NETWORK_PROGRAMS="${NETWORK_PROGRAMS}"$'\n'"FIREWALL_BUTTON ${FIREWALL_ICON} ${FIREWALL_TEXT}"
    fi
    # ADBLOCK_BUTTON #
    ADBLOCK_PROG=/usr/local/bin/block-advert.sh
    ADBLOCK_EXEC="gksu block-advert.sh &"
    ADBLOCK_ICON="$ICONS/advert-block.png"
    ADBLOCK_TEXT=$"Adblock"
    if [ -x $ADBLOCK_PROG ]; then
        NETWORK_PROGRAMS="${NETWORK_PROGRAMS}"$'\n'"ADBLOCK_BUTTON ${ADBLOCK_ICON} ${ADBLOCK_TEXT}"
    fi

## SHARES TAB ##
SHARES_PROGRAMS=""
    # CONNECTSHARES_BUTTON #
    CONNECTSHARES_PROG=/usr/local/bin/connectshares-config
    CONNECTSHARES_EXEC="connectshares-config &"
    CONNECTSHARES_ICON="$ICONS/connectshares-config.png"
    CONNECTSHARES_TEXT=$"ConnectShares Configuration"
    if [ -x $CONNECTSHARES_PROG ]; then
        SHARES_PROGRAMS="${SHARES_PROGRAMS}"$'\n'"CONNECTSHARES_BUTTON ${CONNECTSHARES_ICON} ${CONNECTSHARES_TEXT}"
    fi
    # DISCONNECTSHARES_BUTTON #
    DISCONNECTSHARES_PROG=/usr/local/bin/disconnectshares
    DISCONNECTSHARES_EXEC="disconnectshares &"
    DISCONNECTSHARES_ICON="$ICONS/disconnectshares.png"
    DISCONNECTSHARES_TEXT=$" DisconnectShares"
    if [ -x $DISCONNECTSHARES_PROG ]; then
        SHARES_PROGRAMS="${SHARES_PROGRAMS}"$'\n'"DISCONNECTSHARES_BUTTON ${DISCONNECTSHARES_ICON} ${DISCONNECTSHARES_TEXT}"
    fi
    # DROOPY_BUTTON #
    DROOPY_PROG=/usr/local/bin/droopy.sh
    DROOPY_EXEC="droopy.sh &"
    DROOPY_ICON="$ICONS/droopy.png"
    DROOPY_TEXT=$"Droopy (File Sharing)"
    if [ -x $DROOPY_PROG ]; then
        SHARES_PROGRAMS="${SHARES_PROGRAMS}"$'\n'"DROOPY_BUTTON ${DROOPY_ICON} ${DROOPY_TEXT}"
    fi
    # SSHCONDUIT_BUTTON #
    SSHCONDUIT_PROG=/usr/local/bin/ssh-conduit.sh
    SSHCONDUIT_EXEC="ssh-conduit.sh &"
    SSHCONDUIT_ICON="$ICONS2/ssh-conduit.png"
    SSHCONDUIT_TEXT=$"SSH Conduit"
    if [ -x $SSHCONDUIT_PROG ]; then
        SHARES_PROGRAMS="${SHARES_PROGRAMS}"$'\n'"SSHCONDUIT_BUTTON ${SSHCONDUIT_ICON} ${SSHCONDUIT_TEXT}"
    fi
    # ASSISTANT_BUTTON #
    SSHCONDUIT_PROG=/usr/local/bin/1-to-1_assistance.sh
    SSHCONDUIT_EXEC="1-to-1_assistance.sh &"
    SSHCONDUIT_ICON="$ICONS2/1-to-1_assistance.png"
    SSHCONDUIT_TEXT=$"1-to-1 Assistance"
    if [ -x $SSHCONDUIT_PROG ]; then
        SHARES_PROGRAMS="${SHARES_PROGRAMS}"$'\n'"SSHCONDUIT_BUTTON ${SSHCONDUIT_ICON} ${SSHCONDUIT_TEXT}"
    fi
    # VOICE_BUTTON #
    VOICE_PROG=/usr/local/bin/1-to-1_assistance.sh
    VOICE_EXEC="1-to-1_voice.sh &"
    VOICE_ICON="$ICONS2/1-to-1_voice.png"
    VOICE_TEXT=$"1-to-1 Voice"
    if [ -x $VOICE_PROG ]; then
        SHARES_PROGRAMS="${SHARES_PROGRAMS}"$'\n'"VOICE_BUTTON ${VOICE_ICON} ${VOICE_TEXT}"
    fi

## SESSION TAB ##
SESSION_PROGRAMS=""
    # ARANDR_BUTTON #
    ARANDR_PROG=/usr/bin/arandr
    ARANDR_EXEC="arandr &"
    ARANDR_ICON="$ICONS/video-display.png"
    ARANDR_TEXT=$"Set Screen Resolution (ARandR)"
    if [ -x $ARANDR_PROG ]; then
        SESSION_PROGRAMS="${SESSION_PROGRAMS}"$'\n'"ARANDR_BUTTON ${ARANDR_ICON} ${ARANDR_TEXT}"
    fi
    # GKSU_BUTTON #
    GKSU_PROG=/usr/bin/gksu-properties
    GKSU_EXEC="gksu-properties &"
    GKSU_ICON="$ICONS/gksu.png"
    GKSU_TEXT=$"Password Prompt (su/sudo)"
    if [ -x $GKSU_PROG ]; then
        SESSION_PROGRAMS="${SESSION_PROGRAMS}"$'\n'"GKSU_BUTTON ${GKSU_ICON} ${GKSU_TEXT}"
    fi
    # SCREENBLANK_BUTTON #
    SCREENBLANK_PROG=/usr/local/bin/set-screen-blank
    SCREENBLANK_EXEC="set-screen-blank &"
    SCREENBLANK_ICON="$ICONS/set-screen-blanking.png"
    SCREENBLANK_TEXT=$"Set Screen Blanking"
    if [ -x $SCREENBLANK_PROG ]; then
        SESSION_PROGRAMS="${SESSION_PROGRAMS}"$'\n'"SCREENBLANK_BUTTON ${SCREENBLANK_ICON} ${SCREENBLANK_TEXT}"
    fi
    # LXKEYMAP_BUTTON #
    LXKEYMAP_PROG=/usr/bin/lxkeymap
    LXKEYMAP_EXEC="lxkeymap &"
    LXKEYMAP_ICON="$ICONS/input-keyboard.png"
    LXKEYMAP_TEXT=$"Change Keymap for Session"
    if [ -x $LXKEYMAP_PROG ]; then
        SESSION_PROGRAMS="${SESSION_PROGRAMS}"$'\n'"LXKEYMAP_BUTTON ${LXKEYMAP_ICON} ${LXKEYMAP_TEXT}"
    fi
    # SLIMLOGIN_BUTTON #
    SLIMLOGIN_PROG=/usr/local/bin/slim-login
    SLIMLOGIN_EXEC="gksu slim-login &"
    SLIMLOGIN_ICON="$ICONS/preferences-system-login.png"
    SLIMLOGIN_TEXT=$"Set Auto-Login"
    if [ -x $SLIMLOGIN_PROG ]; then
        SESSION_PROGRAMS="${SESSION_PROGRAMS}"$'\n'"SLIMLOGIN_BUTTON ${SLIMLOGIN_ICON} ${SLIMLOGIN_TEXT}"
    fi
    # DESKTOP_SESSION_BUTTON #
    DESKTOP_SESSION_ICON="$ICONS/preferences-system-session.png"
    DESKTOP_SESSION_TEXT=$"User Desktop-Session"
    if [ -d /usr/share/doc/desktop-session-antix/ ]; then
        SESSION_PROGRAMS="${SESSION_PROGRAMS}"$'\n'"DESKTOP_SESSION_BUTTON ${DESKTOP_SESSION_ICON} ${DESKTOP_SESSION_TEXT}"
    fi
    # SLIM_BACK_BUTTON #
    SLIM_BACK_PROG=/usr/local/bin/antixccslim.sh
    SLIM_BACK_EXEC="gksu antixccslim.sh &"
    SLIM_BACK_ICON="$ICONS/preferences-desktop-wallpaper.png"
    SLIM_BACK_TEXT=$"Change Slim Background"
    if [ -x $SLIM_BACK_PROG ] && [ -x $/usr/bin/slim ]; then
        SESSION_PROGRAMS="${SESSION_PROGRAMS}"$'\n'"SLIM_BACK_BUTTON ${SLIM_BACK_ICON} ${SLIM_BACK_TEXT}"
    fi
    # GRUB_BACK_BUTTON #
    GRUB_BACK_PROG=/usr/local/bin/antixccgrub.sh
    GRUB_BACK_EXEC="gksu antixccgrub.sh &"
    GRUB_BACK_ICON="$ICONS/screensaver.png"
    GRUB_BACK_TEXT=$"Set Grub Boot Image (png only)"
    if [ -x $GRUB_BACK_PROG ]; then
        SESSION_PROGRAMS="${SESSION_PROGRAMS}"$'\n'"GRUB_BACK_BUTTON ${GRUB_BACK_ICON} ${GRUB_BACK_TEXT}"
    fi

## DISKS TAB ##
DISKS_PROGRAMS=""
    # AUTOMOUNT_BUTTON #
    AUTOMOUNT_PROG=/usr/local/bin/automount-config
    AUTOMOUNT_EXEC="automount-config &"
    AUTOMOUNT_ICON="$ICONS/mountbox.png"
    AUTOMOUNT_TEXT=$"Configure Automount"
    if [ -x $AUTOMOUNT_PROG ]; then
        DISKS_PROGRAMS="${DISKS_PROGRAMS}"$'\n'"AUTOMOUNT_BUTTON ${AUTOMOUNT_ICON} ${AUTOMOUNT_TEXT}"
    fi
    # MOUNTBOX_BUTTON #
    MOUNTBOX_PROG=/usr/local/bin/mountbox
    MOUNTBOX_EXEC="mountbox &"
    MOUNTBOX_ICON="$ICONS/mountbox.png"
    MOUNTBOX_TEXT=$"Mount Connected Devices"
    if [ -x $MOUNTBOX_PROG ]; then
        DISKS_PROGRAMS="${DISKS_PROGRAMS}"$'\n'"MOUNTBOX_BUTTON ${MOUNTBOX_ICON} ${MOUNTBOX_TEXT}"
    fi
    # INSTALLER_BUTTON #
    INSTALLER_PROG=/usr/sbin/minstall
    INSTALLER_EXEC="gksu $installer_prog &"
    INSTALLER_ICON="$ICONS2/msystem.png"
    INSTALLER_TEXT=$"Install antiX Linux"
    if [ -x $INSTALLER_PROG ] && $ITS_ALIVE; then
        DISKS_PROGRAMS="${DISKS_PROGRAMS}"$'\n'"INSTALLER_BUTTON ${INSTALLER_ICON} ${INSTALLER_TEXT}"
    fi
    # LIVE_USB_MAKER_BUTTON #
    LIVE_USB_MAKER_PROG=/usr/bin/live-usb-maker-gui-antix
    LIVE_USB_MAKER_EXEC="gksu live-usb-maker-gui-antix &"
    LIVE_USB_MAKER_EXEC2="$terminalexec sudo live-usb-maker &"
    LIVE_USB_MAKER_ICON="$ICONS/live-usb-maker.png"
    if [ -x $LIVE_USB_MAKER_PROG ] || [ -x /usr/local/bin/live-usb-maker ]; then
		if [ -x $LIVE_USB_MAKER_PROG ]; then LIVE_USB_MAKER_TEXT=$"Live USB Maker (gui)";
		else LIVE_USB_MAKER_TEXT=$"Live USB Maker (cli)"; fi
        DISKS_PROGRAMS="${DISKS_PROGRAMS}"$'\n'"LIVE_USB_MAKER_BUTTON ${LIVE_USB_MAKER_ICON} ${LIVE_USB_MAKER_TEXT}"
    fi
    # GPARTED_BUTTON #
    GPARTED_PROG=/usr/sbin/gparted
    GPARTED_EXEC="gksu gparted &"
    GPARTED_ICON="$ICONS/gparted.png"
    GPARTED_TEXT=$"Partition a Drive"
    if [ -x $GPARTED_PROG ]; then
        DISKS_PROGRAMS="${DISKS_PROGRAMS}"$'\n'"GPARTED_BUTTON ${GPARTED_ICON} ${GPARTED_TEXT}"
    fi
    # PARTIMAGE_BUTTON #
    PARTIMAGE_PROG=/usr/sbin/partimage
    PARTIMAGE_EXEC="$terminalexec sudo partimage &"
    PARTIMAGE_ICON="$ICONS/drive-harddisk-system.png"
    PARTIMAGE_TEXT=$"Image a Partition"
    if [ -x $PARTIMAGE_PROG ]; then
        DISKS_PROGRAMS="${DISKS_PROGRAMS}"$'\n'"PARTIMAGE_BUTTON ${PARTIMAGE_ICON} ${PARTIMAGE_TEXT}"
    fi
    # DISK_MANAGER_BUTTON #
    DISK_MANAGER_PROG=/usr/sbin/disk-manager
    DISK_MANAGER_EXEC="gksu disk-manager &"
    DISK_MANAGER_ICON="$ICONS/disk-manager.png"
    DISK_MANAGER_TEXT=$"Manage Disks"
    if [ -x $DISK_MANAGER_PROG ]; then
        DISKS_PROGRAMS="${DISKS_PROGRAMS}"$'\n'"DISK_MANAGER_BUTTON ${DISK_MANAGER_ICON} ${DISK_MANAGER_TEXT}"
    fi
    # GRSYNC_BUTTON #
    GRSYNC_PROG=/usr/bin/grsync
    GRSYNC_EXEC="grsync &"
    GRSYNC_ICON="$ICONS/grsync.png"
    GRSYNC_TEXT=$"Synchronize Directories"
    if [ -x $GRSYNC_PROG ]; then
        DISKS_PROGRAMS="${DISKS_PROGRAMS}"$'\n'"GRSYNC_BUTTON ${GRSYNC_ICON} ${GRSYNC_TEXT}"
    fi

## HARDWARE TAB ##
HARDWARE_PROGRAMS=""
    # INXI_BUTTON #
    INXI_PROG=/usr/local/bin/inxi-gui
    INXI_EXEC="inxi-gui &"
    INXI_ICON="$ICONS/info_blue.png"
    INXI_TEXT=$"PC Information"
    if [ -x $INXI_PROG ]; then
        HARDWARE_PROGRAMS="${HARDWARE_PROGRAMS}"$'\n'"INXI_BUTTON ${INXI_ICON} ${INXI_TEXT}"
    fi
    # MOUSE_BUTTON #
    MOUSE_PROG=/usr/local/bin/ds-mouse
    MOUSE_EXEC="ds-mouse &"
    MOUSE_ICON="$ICONS/input-mouse.png"
    MOUSE_TEXT=$"Mouse Configuration"
    if [ -x $MOUSE_PROG ]; then
        HARDWARE_PROGRAMS="${HARDWARE_PROGRAMS}"$'\n'"MOUSE_BUTTON ${MOUSE_ICON} ${MOUSE_TEXT}"
    fi
    # PRINTER_BUTTON #
    PRINTER_PROG=/usr/bin/system-config-printer
    PRINTER_EXEC="system-config-printer &"
    PRINTER_ICON="$ICONS/printer.png"
    PRINTER_TEXT=$"Print Settings"
    if [ -x $PRINTER_PROG ]; then
        HARDWARE_PROGRAMS="${HARDWARE_PROGRAMS}"$'\n'"PRINTER_BUTTON ${PRINTER_ICON} ${PRINTER_TEXT}"
    fi
    # SOUNDCARD_BUTTON #
    SOUNDCARD_PROG=/usr/local/bin/alsa-set-default-card
    SOUNDCARD_EXEC="alsa-set-default-card &"
    SOUNDCARD_ICON="$ICONS/soundcard.png"
    SOUNDCARD_TEXT=$"Sound Card Chooser"
    if [ -x $SOUNDCARD_PROG ]; then
        HARDWARE_PROGRAMS="${HARDWARE_PROGRAMS}"$'\n'"SOUNDCARD_BUTTON ${SOUNDCARD_ICON} ${SOUNDCARD_TEXT}"
    fi
    # SOUNDTEST_BUTTON #
    SOUNDTEST_PROG=/usr/bin/speaker-test
    SOUNDTEST_EXEC="$terminalexec speaker-test --channels 2 --test wav --nloops 3 &"
    SOUNDTEST_ICON="$ICONS/preferences-desktop-sound.png"
    SOUNDTEST_TEXT=$"Test Sound"
    if [ -x $SOUNDTEST_PROG ]; then
        HARDWARE_PROGRAMS="${HARDWARE_PROGRAMS}"$'\n'"SOUNDTEST_BUTTON ${SOUNDTEST_ICON} ${SOUNDTEST_TEXT}"
    fi
    # MIXER_BUTTON #
    MIXER_PROG=/usr/bin/alsamixer
    MIXER_EXEC="$terminalexec alsamixer &"
    MIXER_ICON="$ICONS/audio-volume-high-panel.png"
    MIXER_TEXT=$"Adjust Mixer"
    if [ -x $MIXER_PROG ]; then
        HARDWARE_PROGRAMS="${HARDWARE_PROGRAMS}"$'\n'"MIXER_BUTTON ${MIXER_ICON} ${MIXER_TEXT}"
    fi
    # EQUIALIZER_BUTTON #
    EQUIALIZER_PROG=/usr/bin/alsamixer
    EQUIALIZER_EXEC="$terminalexec alsamixer -D equalizer &"
    EQUIALIZER_ICON="$ICONS/alsamixer-equalizer.png"
    EQUIALIZER_TEXT=$"Alsamixer Equalizer"
    if [ -x $EQUIALIZER_PROG ]; then
        HARDWARE_PROGRAMS="${HARDWARE_PROGRAMS}"$'\n'"EQUIALIZER_BUTTON ${EQUIALIZER_ICON} ${EQUIALIZER_TEXT}"
    fi
    # BACKLIGHT_BUTTON #
    BACKLIGHT_PROG=/usr/local/bin/backlight-brightness
    BACKLIGHT_EXEC="backlight-brightness &"
    BACKLIGHT_ICON="$ICONS/backlight-brightness.png"
    BACKLIGHT_TEXT=$"Backlight Brightness"
    if [ -x $BACKLIGHT_PROG ] && [ "$(ls -A /sys/class/backlight)" ]; then
        HARDWARE_PROGRAMS="${HARDWARE_PROGRAMS}"$'\n'"BACKLIGHT_BUTTON ${BACKLIGHT_ICON} ${BACKLIGHT_TEXT}"
    fi

## DRIVERS TAB ##
DRIVERS_PROGRAMS=""
    # DDM_BUTTON #
    DDM_PROG=/usr/local/bin/ddm-mx
    DDM_EXEC="$terminalexec su-to-root -c '/usr/local/bin/ddm-mx -i nvidia' &"
    DDM_ICON="$ICONS/nvidia-settings.png"
    DDM_TEXT=$"Nvidia Driver Installer"
    if [ -x $DDM_PROG ]; then
        DRIVERS_PROGRAMS="${DRIVERS_PROGRAMS}"$'\n'"DDM_BUTTON ${DDM_ICON} ${DDM_TEXT}"
    fi
    # NDISWRAPPER_BUTTON #
    NDISWRAPPER_PROG=/usr/sbin/ndisgtk
    NDISWRAPPER_EXEC="gksu /usr/sbin/ndisgtk &"
    NDISWRAPPER_ICON="$ICONS/ndisgtk.png"
    NDISWRAPPER_TEXT=$"Windows Wireless Drivers"
    if [ -x $NDISWRAPPER_PROG ]; then
        DRIVERS_PROGRAMS="${DRIVERS_PROGRAMS}"$'\n'"NDISWRAPPER_BUTTON ${NDISWRAPPER_ICON} ${NDISWRAPPER_TEXT}"
    fi
    # CODECS_BUTTON #
    CODECS_PROG=/usr/sbin/codecs
    CODECS_EXEC="gksu codecs &"
    CODECS_ICON="$ICONS/codecs.png"
    CODECS_TEXT=$"Codecs Installer"
    if [ -x $CODECS_PROG ]; then
        DRIVERS_PROGRAMS="${DRIVERS_PROGRAMS}"$'\n'"CODECS_BUTTON ${CODECS_ICON} ${CODECS_TEXT}"
    fi

## MAINTENANCE TAB ##
MAINTENANCE_PROGRAMS=""
    # SNAPSHOT_BUTTON #
    SNAPSHOT_PROG=/usr/bin/iso-snapshot
    SNAPSHOT_EXEC="gksu iso-snapshot &"
    SNAPSHOT_ICON="$ICONS/gnome-do.png"
    SNAPSHOT_TEXT=$"ISO Snapshot"
    if [ -x $SNAPSHOT_PROG ]; then
        MAINTENANCE_PROGRAMS="${MAINTENANCE_PROGRAMS}"$'\n'"SNAPSHOT_BUTTON ${SNAPSHOT_ICON} ${SNAPSHOT_TEXT}"
    fi
    # BOOT_REPAIR_BUTTON #
    BOOT_REPAIR_PROG=/usr/sbin/bootrepair
    BOOT_REPAIR_EXEC="gksu bootrepair &"
    BOOT_REPAIR_ICON="$ICONS/bootrepair.png"
    BOOT_REPAIR_TEXT=$"Boot Repair"
    if [ -x $BOOT_REPAIR_PROG ]; then
        MAINTENANCE_PROGRAMS="${MAINTENANCE_PROGRAMS}"$'\n'"BOOT_REPAIR_BUTTON ${BOOT_REPAIR_ICON} ${BOOT_REPAIR_TEXT}"
    fi
    # BACKUP_BUTTON #
    BACKUP_PROG=/usr/bin/luckybackup
    BACKUP_EXEC="gksu luckybackup &"
    BACKUP_ICON="$ICONS/luckybackup.png"
    BACKUP_TEXT=$"System Backup"
    if [ -x $BACKUP_PROG ]; then
        MAINTENANCE_PROGRAMS="${MAINTENANCE_PROGRAMS}"$'\n'"BACKUP_BUTTON ${BACKUP_ICON} ${BACKUP_TEXT}"
    fi
    # MENU_MANAGER_BUTTON #
    MENU_MANAGER_PROG=/usr/local/bin/menu_manager.sh
    MENU_MANAGER_EXEC="sudo menu_manager.sh &"
    MENU_MANAGER_ICON="$ICONS/menu-editor.png"
    MENU_MANAGER_TEXT=$"Menu Editor"
    if [ -x $MENU_MANAGER_PROG ]; then
        MAINTENANCE_PROGRAMS="${MAINTENANCE_PROGRAMS}"$'\n'"MENU_MANAGER_BUTTON ${MENU_MANAGER_ICON} ${MENU_MANAGER_TEXT}"
    fi
    # BROADCOM_BUTTON # - Is this relevant today???
    BROADCOM_PROG=/usr/sbin/broadcom-manager
    BROADCOM_EXEC="gksu broadcom-manager &"
    BROADCOM_ICON="$ICONS/palimpsest.png"
    BROADCOM_TEXT=$"Broadcom Manager"
    if [ -x $BROADCOM_PROG ]; then
        MAINTENANCE_PROGRAMS="${MAINTENANCE_PROGRAMS}"$'\n'"BROADCOM_BUTTON ${BROADCOM_ICON} ${BROADCOM_TEXT}"
    fi
    # NET_ASSIST_BUTTON #
    NET_ASSIST_PROG=/usr/sbin/network-assistant
    NET_ASSIST_EXEC="gksu network-assistant &"
    NET_ASSIST_ICON="$ICONS/network-assistant.png"
    NET_ASSIST_TEXT=$"Network Assistant"
    if [ -x $NET_ASSIST_PROG ]; then
        MAINTENANCE_PROGRAMS="${MAINTENANCE_PROGRAMS}"$'\n'"NET_ASSIST_BUTTON ${NET_ASSIST_ICON} ${NET_ASSIST_TEXT}"
    fi
    # USER_MANAGER_BUTTON #
    USER_MANAGER_PROG=/usr/sbin/antix-user
    USER_MANAGER_EXEC="gksu antix-user &"
    USER_MANAGER_ICON="$ICONS/user-manager.png"
    USER_MANAGER_TEXT=$"User Manager"
    if [ -x $USER_MANAGER_PROG ]; then
        MAINTENANCE_PROGRAMS="${MAINTENANCE_PROGRAMS}"$'\n'"USER_MANAGER_BUTTON ${USER_MANAGER_ICON} ${USER_MANAGER_TEXT}"
    fi
    # REPO_MANAGER_BUTTON #
    REPO_MANAGER_PROG=/usr/bin/repo-manager
    REPO_MANAGER_EXEC="gksu repo-manager &"
    REPO_MANAGER_ICON="$ICONS/repo-manager.png"
    REPO_MANAGER_TEXT=$"Repo Manager"
    if [ -x $REPO_MANAGER_PROG ]; then
        MAINTENANCE_PROGRAMS="${MAINTENANCE_PROGRAMS}"$'\n'"REPO_MANAGER_BUTTON ${REPO_MANAGER_ICON} ${REPO_MANAGER_TEXT}"
    fi

## LIVE TAB ##
LIVE_PROGRAMS=""
    # PERSIST_CONF_BUTTON #
    PERSIST_CONF_PROG=/usr/local/bin/persist-config
    PERSIST_CONF_EXEC="gksu persist-config &"
    PERSIST_CONF_ICON="$ICONS/pref.png"
    PERSIST_CONF_TEXT=$"Configure Live Persistence"
    if [ -x $PERSIST_CONF_PROG ]; then
        LIVE_PROGRAMS="${LIVE_PROGRAMS}"$'\n'"PERSIST_CONF_BUTTON ${PERSIST_CONF_ICON} ${PERSIST_CONF_TEXT}"
    fi
    # PERSIST_MAKE_BUTTON #
    PERSIST_MAKE_PROG=/usr/local/bin/persist-makefs
    PERSIST_MAKE_EXEC="gksu persist-makefs &"
    PERSIST_MAKE_ICON="$ICONS/persist-makefs.png"
    PERSIST_MAKE_TEXT=$"Set Up Live Persistence"
    if [ -x $PERSIST_MAKE_PROG ]; then
        LIVE_PROGRAMS="${LIVE_PROGRAMS}"$'\n'"PERSIST_MAKE_BUTTON ${PERSIST_MAKE_ICON} ${PERSIST_MAKE_TEXT}"
    fi
    # PERSIST_SAVE_BUTTON #
    PERSIST_SAVE_PROG=/usr/local/bin/persist-save
    PERSIST_SAVE_EXEC="gksu persist-save &"
    PERSIST_SAVE_ICON="$ICONS/palimpsest.png"
    PERSIST_SAVE_TEXT=$"Save Root Persistence"
    if [ -x $PERSIST_SAVE_PROG ] && [[ -e /etc/live/config/save-persist || -e /etc/live/config/persist-save.conf ]]; then
        LIVE_PROGRAMS="${LIVE_PROGRAMS}"$'\n'"PERSIST_SAVE_BUTTON ${PERSIST_SAVE_ICON} ${PERSIST_SAVE_TEXT}"
    fi
    # LIVE_REMASTER_BUTTON #
    LIVE_REMASTER_PROG=/usr/local/bin/live-remaster
    LIVE_REMASTER_EXEC="gksu live-remaster &"
    LIVE_REMASTER_ICON="$ICONS/remastersys.png"
    LIVE_REMASTER_TEXT=$"Remaster-Customize Live"
    if [ -x $LIVE_REMASTER_PROG ] && [[ -e /etc/live/config/remasterable || -e /etc/live/config/remaster-live.conf ]]; then
        LIVE_PROGRAMS="${LIVE_PROGRAMS}"$'\n'"LIVE_REMASTER_BUTTON ${LIVE_REMASTER_ICON} ${LIVE_REMASTER_TEXT}"
    fi
    # LIVE_KERNEL_BUTTON #
    LIVE_KERNEL_PROG=/usr/local/bin/live-kernel-updater
    LIVE_KERNEL_EXEC="$terminalexec sudo /usr/local/bin/live-kernel-updater --pause &"
    LIVE_KERNEL_ICON="$ICONS/live-usb-kernel-updater.png"
    LIVE_KERNEL_TEXT=$"Live-USB Kernel Updater"
    if [ -x $LIVE_KERNEL_PROG ]; then
        LIVE_PROGRAMS="${LIVE_PROGRAMS}"$'\n'"LIVE_KERNEL_BUTTON ${LIVE_KERNEL_ICON} ${LIVE_KERNEL_TEXT}"
    fi
    # EXCLUDES_BUTTON # excludes_entry
    EXCLUDES_ICON="$ICONS/remastersys.png"
    EXCLUDES_TEXT=$"Edit Exclude Files"
    if [ -d /usr/local/share/excludes ]; then
        LIVE_PROGRAMS="${LIVE_PROGRAMS}"$'\n'"EXCLUDES_BUTTON ${EXCLUDES_ICON} ${EXCLUDES_TEXT}"
    fi
    # BOOTLOADER_BUTTON # bootloader_entry
    BOOTLOADER_ICON="$ICONS/preferences-desktop.png"
    BOOTLOADER_TEXT=$"Edit Bootloader Menu"
    if [ -f /live/boot-dev/boot/syslinux/syslinux.cfg ] || [ -f /live/boot-dev/boot/grub/grub.cfg ]; then
        LIVE_PROGRAMS="${LIVE_PROGRAMS}"$'\n'"BOOTLOADER_BUTTON ${BOOTLOADER_ICON} ${BOOTLOADER_TEXT}"
    fi
    
### Building layouts for each tab ###
DESKTOP_LAYOUT="$(build_layout "DESKTOP" "$DESKTOP_ICON" "$DESKTOP_PROGRAMS")"
SYSTEM_LAYOUT="$(build_layout "SYSTEM" "$SYSTEM_ICON" "$SYSTEM_PROGRAMS")"
NETWORK_LAYOUT="$(build_layout "NETWORK" "$NETWORK_ICON" "$NETWORK_PROGRAMS")"
SHARES_LAYOUT="$(build_layout "SHARES" "$SHARES_ICON" "$SHARES_PROGRAMS")"
SESSION_LAYOUT="$(build_layout "SESSION" "$SESSION_ICON" "$SESSION_PROGRAMS")"
DISKS_LAYOUT="$(build_layout "DISKS" "$DISKS_ICON" "$DISKS_PROGRAMS")"
HARDWARE_LAYOUT="$(build_layout "HARDWARE" "$HARDWARE_ICON" "$HARDWARE_PROGRAMS")"
DRIVERS_LAYOUT="$(build_layout "DRIVERS" "$DRIVERS_ICON" "$DRIVERS_PROGRAMS")"
MAINTENANCE_LAYOUT="$(build_layout "MAINTENANCE" "$MAINTENANCE_ICON" "$MAINTENANCE_PROGRAMS")"
LIVE_LAYOUT="$(build_layout "LIVE" "$LIVE_ICON" "$LIVE_PROGRAMS")"

# If Dialog is resizable
if $DIALOG_RESIZE; then RESIZE="-r"; fi

coproc dialogbox --hidden $RESIZE   # Note: bash supports only one co-process at a time
INPUTFD=${COPROC[0]}  # file descriptor the dialogbox process writes to
OUTPUTFD=${COPROC[1]}  # file descriptor the dialogbox process reads from
DBPID=$COPROC_PID    # PID of the dialogbox, if you need it for any purpose... e.g. to kill it

set -o monitor  # Enable SIGCHLD

# Building the dialogbox
cat >&$OUTPUTFD <<EOCC
add frame LEFT_MENU vertical noframe
	add listbox "$Desktop" LIST_TABS selection
		add item "$Desktop" $DESKTOP_ICON
		add item "$System" $SYSTEM_ICON
		add item "$Network" $NETWORK_ICON
		add item "$Shares" $SHARES_ICON
		add item "$Session" $SESSION_ICON
		add item "$Live" $LIVE_ICON
		add item "$Disks" $DISKS_ICON
		add item "$Hardware" $HARDWARE_ICON
		add item "$Drivers" $DRIVERS_ICON
		add item "$Maintenance" $MAINTENANCE_ICON
	end listbox LIST_TABS

	add pushbutton "Config" CONFIGURE_CC
	set CONFIGURE_CC icon $CONFIGURATION_ICON
end frame LEFT_MENU
set LEFT_MENU stylesheet "QFrame {min-width:9em; max-width:12em}
                          QPushButton {icon-size:18px}
                          QListWidget {icon-size:18px; text-align:left;
                              min-width:7em; max-width:11em;
                              padding:3px; min-height:13em}"
step horizontal

add frame EMPTY_LEFT vertical noframe
end frame EMPTY_LEFT
set EMPTY_LEFT stylesheet "min-width:1px;max-width:2px;border: 0px"
hide EMPTY_LEFT
step horizontal

$TABS_START

$MAINTENANCE_LAYOUT
$DRIVERS_LAYOUT
$HARDWARE_LAYOUT
$DISKS_LAYOUT
$LIVE_LAYOUT
$SESSION_LAYOUT
$SHARES_LAYOUT
$NETWORK_LAYOUT
$SYSTEM_LAYOUT
$DESKTOP_LAYOUT

$TABS_END

set stylesheet " QPushButton {icon-size:${BUTTON_ICONSIZE}px;}"
set title "antiX Control Centre"
set icon "/usr/share/icons/papirus-antix/48x48/apps/control-center2.png"
set class "antix-control-centre"
show
EOCC

# Hide Tabbar
if $USE_TABS && $HIDE_TABBAR; then
	echo "set CC_TABS stylesheet \" QTabBar::tab {max-height:0px;}\"" >&$OUTPUTFD
fi

# Activate left and right stretch
if $LEFT_STRETCH; then echo "show EMPTY_LEFT" >&$OUTPUTFD; fi

# If not live, remove live entry
if ! $ITS_ALIVE; then
	for (( i=1; i<=$CC_COLUMNS; i++ )); do
		echo "disable LIVE_${i}" >&$OUTPUTFD
	done
	#if $USE_TABS; then echo "disable LIVE_TAB" >&$OUTPUTFD; fi
fi

# Show only the selected layout
echo "set \"LIST_TABS:$SHOW_TAB\" current" >&$OUTPUTFD
show_layout "$SHOW_TAB"

while IFS=$'=' read key value; do
  case $key in
## CHANGE TAB ##
    LIST_TABS)               show_layout "$value" "$SHOW_TAB"; SHOW_TAB="$value";;
## DESKTOP TAB ##
    WALLPAPER_BUTTON)        bash -c "$WALLPAPER_EXEC";;
    DPI_BUTTON)              bash -c "$DPI_EXEC";;
    LXAPPEARANCE_BUTTON)     bash -c "$LXAPPEARANCE_EXEC";;
    ICEWM_CONFIG_BUTTON)     yad_textfile "icewm";;
    JWM_CONFIG_BUTTON)       yad_textfile "jwm";;
    FLUX_CONFIG_BUTTON)      yad_textfile "fluxbox";;
    CONKY_BUTTON)            yad_textfile "conky";;
    PREFAPPS_BUTTON)         bash -c "$PREFAPPS_EXEC";;
## SYSTEM TAB ##
    SYNAPTIC_BUTTON)         if [ -x $SYNAPTIC_PROG ]; then
                                 bash -c "$SYNAPTIC_EXEC"
                             else 
                                 bash -c "$SYNAPTIC_EXEC2"
                             fi;;
    PACKAGE_INST_BUTTON)     bash -c "$PACKAGE_INST_EXEC";;
    CONFIG_FILES_BUTTON)     yad_textfile "config-files";;
    FSKB_SETTINGS_BUTTON)    bash -c "$FSKB_SETTINGS_EXEC";;
    SYSV_CONF_BUTTON)        bash -c "$SYSV_CONF_EXEC";;
    TZDATA_BUTTON)           if [ -x $TZDATA_PROG ]; then
                                 bash -c "$TZDATA_EXEC"
                             else 
                                 bash -c "$TZDATA_EXEC2"
                             fi;;
    GALTERNATIVES_BUTTON)    bash -c "$GALTERNATIVES_EXEC";;
## NETWORK TAB ##
    CONNMAN_BUTTON)          if [ -x /usr/local/bin/antix-wifi-switch ]; then
                                 bash -c "$CONNMAN_EXEC"
                             else 
                                 bash -c "$CONNMAN_EXEC2"
                             fi;;
                             
    CENI_BUTTON)             if [ -x /usr/local/bin/antix-wifi-switch ]; then
                                 bash -c "$CENI_EXEC"
                             else 
                                 bash -c "$CENI_EXEC2"
                             fi;;
    WPASUPPLICANT_BUTTON)    bash -c "$WPASUPPLICANT_EXEC";;
    UMTS_BUTTON)             bash -c "$UMTS_EXEC";;
    GNOMEPPP_BUTTON)         bash -c "$GNOMEPPP_EXEC";;
    PPPOE_CONF_BUTTON)       bash -c "$PPPOE_CONF_EXEC";;
    FIREWALL_BUTTON)         bash -c "$FIREWALL_EXEC";;
    ADBLOCK_BUTTON)          bash -c "$ADBLOCK_EXEC";;
## SHARES TAB ##
    CONNECTSHARES_BUTTON)    bash -c "$CONNECTSHARES_EXEC";;
    DISCONNECTSHARES_BUTTON) bash -c "$DISCONNECTSHARES_EXEC";;
    DROOPY_BUTTON)           bash -c "$DROOPY_EXEC";;
    SSHCONDUIT_BUTTON)       bash -c "$SSHCONDUIT_EXEC";;
    ASSISTANT_BUTTON)        bash -c "$ASSISTANT_EXEC";;
    VOICE_BUTTON)            bash -c "$VOICE_EXEC";;
## SESSION TAB ##
    ARANDR_BUTTON)           bash -c "$ARANDR_EXEC";;
    SCREENBLANK_BUTTON)      bash -c "$SCREENBLANK_EXEC";;
    GKSU_BUTTON)             bash -c "$GKSU_EXEC";;
    LXKEYMAP_BUTTON)         bash -c "$LXKEYMAP_EXEC";;
    SLIMLOGIN_BUTTON)        bash -c "$SLIMLOGIN_EXEC";;
    DESKTOP_SESSION_BUTTON)  yad_textfile "desktop-session";;
    SLIM_BACK_BUTTON)        bash -c "$SLIM_BACK_EXEC";;
    GRUB_BACK_BUTTON)        bash -c "$GRUB_BACK_EXEC";;
## DISKS TAB ##
    AUTOMOUNT_BUTTON)        bash -c "$AUTOMOUNT_EXEC";;
    MOUNTBOX_BUTTON)         bash -c "$MOUNTBOX_EXEC";;
    INSTALLER_BUTTON)        bash -c "$INSTALLER_EXEC";;
    LIVE_USB_MAKER_BUTTON)   if [ -x $LIVE_USB_MAKER_PROG ]; then
                                 bash -c "$LIVE_USB_MAKER_EXEC"
                             else 
                                 bash -c "$LIVE_USB_MAKER_EXEC2"
                             fi;;
    GPARTED_BUTTON)          bash -c "$GPARTED_EXEC";;
    PARTIMAGE_BUTTON)        bash -c "$PARTIMAGE_EXEC";;
    DISK_MANAGER_BUTTON)     bash -c "$DISK_MANAGER_EXEC";;
    GRSYNC_BUTTON)           bash -c "$GRSYNC_EXEC";;
## HARDWARE TAB ##
    INXI_BUTTON)             bash -c "$INXI_EXEC";;
    MOUSE_BUTTON)            bash -c "$MOUSE_EXEC";;
    PRINTER_BUTTON)          bash -c "$PRINTER_EXEC";;
    SOUNDCARD_BUTTON)        bash -c "$SOUNDCARD_EXEC";;
    SOUNDTEST_BUTTON)        bash -c "$SOUNDTEST_EXEC";;
    MIXER_BUTTON)            bash -c "$MIXER_EXEC";;
    EQUIALIZER_BUTTON)       bash -c "$EQUIALIZER_EXEC";;
    BACKLIGHT_BUTTON)        bash -c "$BACKLIGHT_EXEC";;
## DRIVERS TAB ##
    DDM_BUTTON)              bash -c "$DDM_EXEC";;
    NDISWRAPPER_BUTTON)      bash -c "$NDISWRAPPER_EXEC";;
    CODECS_BUTTON)           bash -c "$CODECS_EXEC";;
## MAINTENANCE TAB ##
    SNAPSHOT_BUTTON)         bash -c "$SNAPSHOT_EXEC";;
    BOOT_REPAIR_BUTTON)      bash -c "$BOOT_REPAIR_EXEC";;
    BACKUP_BUTTON)           bash -c "$BACKUP_EXEC";;
    MENU_MANAGER_BUTTON)     bash -c "$MENU_MANAGER_EXEC";;
    BROADCOM_BUTTON)         bash -c "$BROADCOM_EXEC";;
    NET_ASSIST_BUTTON)       bash -c "$NET_ASSIST_EXEC";;
    USER_MANAGER_BUTTON)     bash -c "$USER_MANAGER_EXEC";;
    REPO_MANAGER_BUTTON)     bash -c "$REPO_MANAGER_EXEC";;
## LIVE TAB ##
    PERSIST_CONF_BUTTON)     bash -c "$PERSIST_CONF_EXEC";;
    PERSIST_MAKE_BUTTON)     bash -c "$PERSIST_MAKE_EXEC";;
    PERSIST_SAVE_BUTTON)     bash -c "$PERSIST_SAVE_EXEC";;
    LIVE_REMASTER_BUTTON)    bash -c "$LIVE_REMASTER_EXEC";;
    LIVE_KERNEL_BUTTON)      bash -c "$LIVE_KERNEL_EXEC";;
    EXCLUDES_BUTTON)         yad_textfile "excludes";;
    BOOTLOADER_BUTTON)       yad_textfile "bootloader";;
  esac
done <&$INPUTFD
  
set +o monitor  # Disable SIGCHLD
wait $DBPID    # Wait for the user to complete the dialog
