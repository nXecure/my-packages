Format: 3.0 (native)
Source: clipski
Binary: clipski
Architecture: any
Version: 1.4.6
Maintainer: skidoo <email@redact.ed>
Homepage: https://gitlab.com/skidoo/clipski
Standards-Version: 4.1.4
Build-Depends: debhelper-compat (= 11), intltool, libgtk-3-dev
Package-List:
 clipski deb misc optional arch=any
Checksums-Sha1:
 79f14c6c301eededaf6c625141bb4d9bdadc8ce3 158184 clipski_1.4.6.tar.xz
Checksums-Sha256:
 17c677022cfd4a746921b97a00067cf5e3e5f56870c1c663fc173138878c0401 158184 clipski_1.4.6.tar.xz
Files:
 24fdae0aa4985c787d3c9ef103c74f85 158184 clipski_1.4.6.tar.xz
