Format: 3.0 (native)
Source: clipski
Binary: clipski
Architecture: any
Version: 1.4.6
Maintainer: skidoo <email@redact.ed>
Homepage: https://gitlab.com/skidoo/clipski
Standards-Version: 4.1.4
Build-Depends: debhelper-compat (= 12), intltool, libgtk-3-dev
Package-List:
 clipski deb misc optional arch=any
Checksums-Sha1:
 269662c175b16e0392d66aeda99a2fe8c7a2e364 158236 clipski_1.4.6.tar.xz
Checksums-Sha256:
 558f9d17107c7572052cd513009b31436ba9c1f9d965054b97e9b51a9755fa52 158236 clipski_1.4.6.tar.xz
Files:
 4f79dbe7fe0d18186d9356435a255153 158236 clipski_1.4.6.tar.xz
