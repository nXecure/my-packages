Format: 3.0 (quilt)
Source: connman
Binary: connman, connman-vpn, connman-dev, connman-doc
Architecture: linux-any all
Version: 1.36-2.3.0antix2
Maintainer: Alexander Sack <asac@debian.org>
Uploaders: Alf Gaida <agaida@siduction.org>
Homepage: https://01.org/connman
Standards-Version: 4.2.1
Vcs-Browser: https://salsa.debian.org/debian/connman
Vcs-Git: https://salsa.debian.org/debian/connman.git
Build-Depends: debhelper (>= 11~), libudev-dev, libglib2.0-dev, libdbus-1-dev, gtk-doc-tools, libgnutls28-dev, libreadline-dev, libxtables-dev, wpasupplicant, libbluetooth-dev, ppp-dev (>= 2.4.7-2+)
Package-List:
 connman deb net optional arch=linux-any
 connman-dev deb devel optional arch=linux-any
 connman-doc deb doc optional arch=all
 connman-vpn deb net optional arch=linux-any
Checksums-Sha1:
 8f29df34231c214c06ee6eca9ed7c233212b8d12 692616 connman_1.36.orig.tar.xz
 d8f4d4ec4a9a827b7681ac5386d01a493a3148c2 16784 connman_1.36-2.3.0antix2.debian.tar.xz
Checksums-Sha256:
 c789db41cc443fa41e661217ea321492ad59a004bebcd1aa013f3bc10a6e0074 692616 connman_1.36.orig.tar.xz
 2425289b4ea69ed846218663827214c05c9562672c4fd9341aa09eaff0f73642 16784 connman_1.36-2.3.0antix2.debian.tar.xz
Files:
 dae77d9c904d2c223ae849e32079d57e 692616 connman_1.36.orig.tar.xz
 7146736cc4b903903fa1028fdb809a87 16784 connman_1.36-2.3.0antix2.debian.tar.xz
