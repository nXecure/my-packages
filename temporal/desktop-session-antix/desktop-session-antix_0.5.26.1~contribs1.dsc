Format: 3.0 (native)
Source: desktop-session-antix
Binary: desktop-session-antix
Architecture: all
Version: 0.5.26.1~contribs1
Maintainer: anticapitalista <anticapitalista@riseup.net>
Homepage: https://gitlab.com/antiX-Linux/desktop-session-antix
Standards-Version: 4.1.3
Build-Depends: debhelper (>= 9.20120115)
Package-List:
 desktop-session-antix deb system required arch=all
Checksums-Sha1:
 fb6f5ae3c837e0c6200a25fc88b25684922ff480 201568 desktop-session-antix_0.5.26.1~contribs1.tar.xz
Checksums-Sha256:
 99383678c342ba83fded25d9e45e788abc91b142d97578c206858670bcc7c6ab 201568 desktop-session-antix_0.5.26.1~contribs1.tar.xz
Files:
 87b36c770ad16a02b79af522ed351cb6 201568 desktop-session-antix_0.5.26.1~contribs1.tar.xz
