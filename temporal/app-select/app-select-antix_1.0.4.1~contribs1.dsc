Format: 3.0 (native)
Source: app-select-antix
Binary: app-select-antix
Architecture: all
Version: 1.0.4.1~contribs1
Maintainer: anticapitalista <anticapitalista@riseup.net>
Homepage: https://gitlab.com/antiX-Dave/app-select
Standards-Version: 4.1.3
Build-Depends: debhelper (>= 9.20120115)
Package-List:
 app-select-antix deb system optional arch=all
Checksums-Sha1:
 a2f751aa17f98e71f2ee7dd77725cff600881522 8672 app-select-antix_1.0.4.1~contribs1.tar.xz
Checksums-Sha256:
 24302dd90129992958ff4c7e9de1e936c07153e09ffb589179cd81e996fff356 8672 app-select-antix_1.0.4.1~contribs1.tar.xz
Files:
 74d0a9e8f24583841b0211c5178351aa 8672 app-select-antix_1.0.4.1~contribs1.tar.xz
