Format: 3.0 (native)
Source: contribs-goodies
Binary: contribs-goodies
Architecture: all
Version: 0.1.0
Maintainer: nXecure <not@telling.you>
Homepage: https://gitlab.com/antix-contribs/contribs-goodies
Standards-Version: 4.1.3
Build-Depends: debhelper (>= 9)
Package-List:
 contribs-goodies deb system optional arch=all
Checksums-Sha1:
 59fb617843ff7a5c756b76e9d2e277c72b4442b6 27548 contribs-goodies_0.1.0.tar.xz
Checksums-Sha256:
 68613f3341aab8f96d937a42a16b87bfa6806cdcb7fd005f183d02881d208040 27548 contribs-goodies_0.1.0.tar.xz
Files:
 f6d2655a733b1763670e20473d1fdb7d 27548 contribs-goodies_0.1.0.tar.xz
