Format: 3.0 (quilt)
Source: slimski
Binary: slimski
Architecture: any
Version: 1.5.0-1~contribs1
Maintainer: skidoo <email@redact.ed>
Homepage: https://gitlab.com/skidoo/slimski
Standards-Version: 3.9.8
Build-Depends: debconf, debhelper (>= 9), dh-exec, po-debconf, sharutils, cmake, libpam0g-dev, libjpeg-dev, libpng-dev, libxft-dev, libxmu-dev
Package-List:
 slimski deb x11 optional arch=any
Checksums-Sha1:
 610fb170b255880e975464a626694e7c1c2aeb9d 3242099 slimski_1.5.0.orig.tar.gz
 6e8f218ca5924b237529d0cf58732f8fb5371576 21792 slimski_1.5.0-1~contribs1.debian.tar.xz
Checksums-Sha256:
 f2d0007101f4e09892daa6107802cbaf88fbc89065ad775aa243484993dd7c28 3242099 slimski_1.5.0.orig.tar.gz
 ac531792696c2fd1087a7a2628464848320474f704845643d0dae4ad9226c3d4 21792 slimski_1.5.0-1~contribs1.debian.tar.xz
Files:
 9543d1f676d9717f6d337dd32bd10177 3242099 slimski_1.5.0.orig.tar.gz
 1546b05b313e710c9b93948c69b1b0c3 21792 slimski_1.5.0-1~contribs1.debian.tar.xz
